package com.arpang.select_picture.widght.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.arpang.select_picture.R;

/**
 * Created by Administrator on 2016/9/17.
 */
public class SelectPicDialog extends Dialog implements View.OnClickListener{

    Context context;
    Button btn1,btn2,btn3;
    SelectDialogListener listener;

    public SelectPicDialog(Context context,SelectDialogListener listener) {
        super(context, R.style.NoticeDialog);
        this.context = context;
        this.listener = listener;
    }

    public interface  SelectDialogListener {
        public void onClick(View view);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_picture);
        findViewById();
    }

    public void findViewById(){
        btn1 = (Button) findViewById(R.id.button1);
        btn1.setOnClickListener(this);
        btn2 = (Button) findViewById(R.id.button2);
        btn2.setOnClickListener(this);
        btn3 = (Button) findViewById(R.id.button3);
        btn3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
       listener.onClick(v);
    }
}
