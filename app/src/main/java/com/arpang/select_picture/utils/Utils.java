package com.arpang.select_picture.utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.arpang.select_picture.module.picture.picutils.Bimp;

import java.io.IOException;

/**
 * 通用方法
 */
public class Utils {

    public static Bitmap getBitmap(String imagePath,Context mContext) {
        Bitmap bitmap = null;
        try {
            bitmap = Bimp.revitionImageSize(imagePath, mContext);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
