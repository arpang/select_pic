package com.arpang.select_picture.module.home;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.arpang.select_picture.R;
import com.arpang.select_picture.module.picture.picbean.ImageItem;
import com.arpang.select_picture.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/9/16.
 */
public class PicShowAdapter extends BaseAdapter{

    Context context;
    List<ImageItem> list = new ArrayList<>();

    public PicShowAdapter(Context context,List<ImageItem> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        if (list.size() == 6) {
            return 6;
        }
        return list.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.plugin_camera_item_add, null);
            holder = new ViewHolder();
            holder.image = (ImageView) convertView.findViewById(R.id.item_pic);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position == list.size()) {
            /**
             * 增加按钮
             */
            holder.image.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.image.setImageResource(R.drawable.plugin_camera_bg_add);
            if (position == 6) {
                holder.image.setVisibility(View.GONE);
            }
        } else {
            /**
             * 显示选择图片
             */
            holder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //TODO
            holder.image.setImageBitmap(Utils.getBitmap(list.get(position).getImagePath(),context));
            Log.i("qq",list.get(position).getImagePath());
        }
        return convertView;
    }

    class ViewHolder{
        ImageView image;
    }
}
