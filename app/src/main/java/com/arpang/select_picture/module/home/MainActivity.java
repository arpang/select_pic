package com.arpang.select_picture.module.home;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.arpang.select_picture.R;
import com.arpang.select_picture.app.base.BaseActivity;
import com.arpang.select_picture.config.Config;
import com.arpang.select_picture.module.picture.activity.AlbumActivity;
import com.arpang.select_picture.module.picture.picutils.Bimp;
import com.arpang.select_picture.module.picture.picutils.BitmapHelper;
import com.arpang.select_picture.module.picture.picbean.ImageItem;
import com.arpang.select_picture.module.picture.picbean.PublicWay;
import com.arpang.select_picture.module.picture.activity.ShowActivity;
import com.arpang.select_picture.widght.dialog.SelectPicDialog;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends BaseActivity implements View.OnClickListener,AdapterView.OnItemClickListener {

    Button btn;
    GridView gridView;
    List<ImageItem> picList = new ArrayList<>();
    PicShowAdapter adapter;
    String LocalImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bimp bimp = new Bimp();
        bimp.tempSelectBitmap = new ArrayList<>();
        findViewById();
    }

    public void findViewById(){
        btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(this);
        gridView = (GridView) findViewById(R.id.gridview);
        gridView.setOnItemClickListener(this);
        adapter = new PicShowAdapter(this,picList);
        gridView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn:
                Intent intent = new Intent(this, AlbumActivity.class);
                startActivityForResult(intent, 2);
                break;
        }
    }

    // 回调方法，从第二个页面回来的时候会执行这个方法
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            /**
             * 拍照
             */
            final File f = new File(Config.FILE_PIC_SCREENSHOT, LocalImage);  //地址
            if (f.getAbsolutePath() != null) {
                String path = BitmapHelper.getimage(MainActivity.this, f.getAbsolutePath(), false);
                ImageItem imageItem = new ImageItem();
                imageItem.setImagePath(path);
                Bimp.tempSelectBitmap.add(imageItem);
                picList.add(imageItem);
                adapter.notifyDataSetChanged();
            }
        }else if (requestCode == 2 && resultCode == 1) {
            /**
             * 选择图片返回
             */
            picList.clear();
            for (int i = 0; i < Bimp.tempSelectBitmap.size(); i++) {
                picList.add(Bimp.tempSelectBitmap.get(i));
            }
            adapter = new PicShowAdapter(this,picList);
            gridView.setAdapter(adapter);
        }else if (requestCode == 3&&resultCode == 1){
            picList = (List<ImageItem>) data.getSerializableExtra("list");
            adapter = new PicShowAdapter(this,picList);
            gridView.setAdapter(adapter);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == picList.size()) {
            //选择图片
            selectPic();
        } else {
            Intent intent = new Intent(this, ShowActivity.class);
            intent.putExtra("position", position);
            intent.putExtra("list", (Serializable) picList);
            startActivityForResult(intent, 3);
        }
    }

    /**
     * 选择图片弹出框
     */
    SelectPicDialog selectDialog;
    public void selectPic() {
        selectDialog = new SelectPicDialog(this, new SelectPicDialog.SelectDialogListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (view.getId() == R.id.button1) {
                        takePhoto(); //拍照
                        selectDialog.dismiss();
                    } else if (view.getId() == R.id.button2) {
                        PublicWay.num = 6 - picList.size() + Bimp.tempSelectBitmap.size();
                        Intent intent = new Intent(MainActivity.this, AlbumActivity.class);
                        startActivityForResult(intent, 2);
                        selectDialog.dismiss();
                    } else if (view.getId() == R.id.button3) {
                        selectDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        selectDialog.setContentView(R.layout.dialog_select_picture);
        selectDialog.show();
        //解决弹出框不全屏问题
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = selectDialog.getWindow().getAttributes();
        lp.width = dm.widthPixels; //设置宽度
        selectDialog.getWindow().setAttributes(lp);
    }

    /**
     * 拍照跳转
     */
    public void takePhoto() {
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED)) {
            try {
                LocalImage = String.valueOf((new Date()).getTime()) + ".jpg";
                File filePath = Config.FILE_PIC_SCREENSHOT;
                if (!filePath.exists()) {
                    filePath.mkdirs();
                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(filePath, LocalImage);
                Uri u = Uri.fromFile(f);
                intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, u);
                startActivityForResult(intent, 1);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
