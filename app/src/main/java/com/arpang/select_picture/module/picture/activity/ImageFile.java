package com.arpang.select_picture.module.picture.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.TextView;


import com.arpang.select_picture.R;
import com.arpang.select_picture.module.picture.picbean.ImageItem;
import com.arpang.select_picture.module.picture.picbean.PublicWay;
import com.arpang.select_picture.module.picture.picadapter.FolderAdapter;

import java.util.ArrayList;


/**
 * 这个类主要是用来进行显示包含图片的文件夹
 *
 * @author king
 * @version 2014年10月18日  下午11:48:06
 */
public class ImageFile extends Activity implements FolderAdapter.ListItemClickHelp {

	private FolderAdapter folderAdapter;
	private TextView btn_back;
	private Context mContext;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plugin_camera_image_file);
		PublicWay.activityList.add(this);
		mContext = this;
		btn_back = (TextView) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new CancelListener());
		GridView gridView = (GridView) findViewById(R.id.fileGridView);
		TextView textView = (TextView) findViewById(R.id.headerTitle);
		textView.setText(R.string.photo);
		folderAdapter = new FolderAdapter(this,this);
		gridView.setAdapter(folderAdapter);
	}

	@Override
	public void onClick(View item, View widget, int position, int which) {
		ShowAllPhoto.dataList = (ArrayList<ImageItem>) AlbumActivity.contentList.get(position).imageList;
		Intent intent = new Intent();
		String folderName = AlbumActivity.contentList.get(position).bucketName;
		intent.putExtra("folderName", folderName);
		intent.setClass(ImageFile.this, ShowAllPhoto.class);
		ImageFile.this.startActivityForResult(intent,1);
	}

	private class CancelListener implements OnClickListener {// 取消按钮的监听
		public void onClick(View v) {
			//清空选择的图片
//			Bimp.tempSelectBitmap.clear();
//			Intent intent = new Intent();
//			intent.setClass(mContext, NoticeStudentActivity.class);
//			startActivity(intent);
			ImageFile.this.setResult(1,new Intent());
			ImageFile.this.finish();
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
//			Intent intent = new Intent();
//			intent.setClass(mContext, NoticeStudentActivity.class);
//			startActivity(intent);
			ImageFile.this.setResult(1,new Intent());
			ImageFile.this.finish();
		}

		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1&&resultCode == 88){
			ImageFile.this.setResult(88,new Intent());
			ImageFile.this.finish();
		}
	}
}
