package com.arpang.select_picture.module.picture.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;

import com.arpang.select_picture.R;
import com.arpang.select_picture.module.picture.picutils.Bimp;
import com.arpang.select_picture.module.picture.picbean.ImageItem;
import com.arpang.select_picture.module.picture.picutils.PhotoView;
import com.arpang.select_picture.module.picture.picbean.PublicWay;
import com.arpang.select_picture.module.picture.picutils.ViewPagerFixed;
import com.arpang.select_picture.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * 招生编辑图片浏览及删除
 */
public class ShowActivity extends Activity implements View.OnClickListener{

    Button btn_finish;
    Button btn_delete;

    int position;     //获取前一个activity传过来的position
    List<ImageItem> picList = new ArrayList<>();
    int location = 0;//当前的位置

    private ArrayList<View> viewList = null;
    private ViewPagerFixed pager;
    private MyPageAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plugin_camera_gallery);
        setEvent();

        position = getIntent().getIntExtra("position", 0);
        picList = (List<ImageItem>) getIntent().getSerializableExtra("list");
        /**
         * 显示结束的按钮
         */
        button_ok();
        /**
         *滑动
         */
        pager = (ViewPagerFixed) findViewById(R.id.gallery01);
        pager.addOnPageChangeListener(pageChangeListener);
        for (int i = 0; i < picList.size(); i++) {
            if (viewList == null)
                viewList = new ArrayList<View>();
            PhotoView img = new PhotoView(this);
            img.setBackgroundColor(0xff000000);
            img.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            ImageItem imageItem = picList.get(i);
            //TODO
            img.setImageBitmap(Utils.getBitmap(imageItem.getImagePath(),this));
            viewList.add(img);
        }

        adapter = new MyPageAdapter(viewList);
        pager.setAdapter(adapter);
        pager.setPageMargin((int) getResources().getDimensionPixelOffset(R.dimen.ui_10_dip));
        pager.setCurrentItem(position);
    }

    public void setEvent() {
        btn_finish = (Button) findViewById(R.id.send_button);
        btn_delete = (Button) findViewById(R.id.gallery_del);
        btn_finish.setOnClickListener(this);
        btn_delete.setOnClickListener(this);
    }

    private OnPageChangeListener pageChangeListener = new OnPageChangeListener() {

        public void onPageSelected(int arg0) {
            location = arg0;
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        public void onPageScrollStateChanged(int arg0) {

        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.send_button:
                Intent intent = new Intent();
                intent.putExtra("list", (Serializable) picList);
                ShowActivity.this.setResult(1, intent);
                ShowActivity.this.finish();
                break;
            case R.id.gallery_del:
                delete();
                break;
        }
    }

    public void delete(){
        if (viewList.size() == 1) {
            Bimp.tempSelectBitmap.clear();
            picList.clear();
            Bimp.max = 0;
            btn_finish.setText("完成"+ "(" + picList.size() + "/" + PublicWay.num + ")");
            Intent intent = new Intent();
            intent.putExtra("picList", (Serializable) picList);
            ShowActivity.this.setResult(1, intent);
            ShowActivity.this.finish();
        } else {
            Bimp.tempSelectBitmap.remove(location);
            picList.remove(location);
            Bimp.max--;
            pager.removeAllViews();
            viewList.remove(location);
            adapter.setListViews(viewList);
            btn_finish.setText("完成" + "(" + picList.size() + "/" + PublicWay.num + ")");
            adapter.notifyDataSetChanged();
        }
    }



    public void button_ok() {
        if (picList.size() > 0) {
            btn_finish.setText("完成" + "(" + picList.size() + "/" + PublicWay.num + ")");
            btn_finish.setPressed(true);
            btn_finish.setClickable(true);
            btn_finish.setTextColor(Color.WHITE);
        } else {
            btn_finish.setPressed(false);
            btn_finish.setClickable(false);
            btn_finish.setTextColor(Color.parseColor("#E1E0DE"));
        }
    }

    /**
     * 监听返回按钮
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent();
            intent.putExtra("list", (Serializable) picList);
            ShowActivity.this.setResult(1, intent);
            ShowActivity.this.finish();
        }
        return true;
    }


    class MyPageAdapter extends PagerAdapter {

        private ArrayList<View> listViews;

        private int size;

        public MyPageAdapter(ArrayList<View> listViews) {
            this.listViews = listViews;
            size = listViews == null ? 0 : listViews.size();
        }

        public void setListViews(ArrayList<View> listViews) {
            this.listViews = listViews;
            size = listViews == null ? 0 : listViews.size();
        }

        public int getCount() {
            return size;
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        public void destroyItem(View arg0, int arg1, Object arg2) {
            ((ViewPagerFixed) arg0).removeView(listViews.get(arg1 % size));
        }

        public void finishUpdate(View arg0) {
        }

        public Object instantiateItem(View arg0, int arg1) {
            try {
                ((ViewPagerFixed) arg0).addView(listViews.get(arg1 % size), 0);

            } catch (Exception e) {
            }
            return listViews.get(arg1 % size);
        }

        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

    }
}
