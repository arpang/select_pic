package com.arpang.select_picture.module.picture.picbean;

import java.io.Serializable;

/**
 * Created by snow on 2015/10/22.
 */
public class ImageBean implements Serializable{

    private String key = "";
    private String bucket = "";

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }
}
