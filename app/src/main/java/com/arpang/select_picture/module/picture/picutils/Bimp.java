package com.arpang.select_picture.module.picture.picutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.arpang.select_picture.module.picture.picbean.ImageItem;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Bimp {
	public static int max = 0;
	
	public static ArrayList<ImageItem> tempSelectBitmap = new ArrayList<ImageItem>();   //选择的图片的临时列表
	public static String smallPath="";


//	public static Bitmap revitionImageSize(String path){
//		Bitmap bitmap = null;
//
//		BitmapFactory.Options  options = new BitmapFactory.Options();
//		options.inJustDecodeBounds = true;
//		BitmapFactory.decodeFile(path,options);
//		int height = options.outHeight;
//		int width = options.outWidth;
//		int inSampleSize = 1;
//		int reqHeight=800;
//		int reqWidth=480;
//		if (height > reqHeight || width > reqWidth) {
//			final int heightRatio = Math.round((float) height/ (float) reqHeight);
//			final int widthRatio = Math.round((float) width / (float) reqWidth);
//			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
//		}
//		options.inSampleSize = calculateInSampleSize(options, 480, 800);
//		options.inJustDecodeBounds = false;
//		 bitmap= BitmapFactory.decodeFile(path, options);
//
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		bitmap.compress(Bitmap.CompressFormat.JPEG, 60, baos);
//		byte[] b = baos.toByteArray();
//
//
//		return bitmap;
//	}
//	public static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {
//		final int height = options.outHeight;
//		final int width = options.outWidth;
//		int inSampleSize = 1;
//
//		if (height > reqHeight || width > reqWidth) {
//			final int heightRatio = Math.round((float) height/ (float) reqHeight);
//			final int widthRatio = Math.round((float) width / (float) reqWidth);
//			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
//		}
//		return inSampleSize;
//	}



	public static Bitmap revitionImageSize (String path,Context context) throws IOException {
		final  File f = new File(path);
//		 smallPath = BitmapHelper.compressBitmap(context,f.getAbsolutePath(),0,0,false);
		smallPath = BitmapHelper.getimage(context,f.getAbsolutePath(),false);
		Bitmap bitmap = BitmapFactory.decodeFile(smallPath);
//		ImageItem imageItem = new ImageItem();
//		imageItem.setBitmap(bitmap);
//		imageItem.setImagePath(smallPath);
//		Bimp.tempSelectBitmap.set(Bimp.tempSelectBitmap.size()-1,imageItem);
//		Bimp.tempSelectBitmap.add(imageItem);




		BufferedInputStream in = new BufferedInputStream(new FileInputStream(
				new File(path)));
		Log.i("paasdf",path);
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(in, null, options);
		in.close();
		int i = 0;
		while (true) {
			if ((options.outWidth >> i <= 1000)
					&& (options.outHeight >> i <= 1000)) {
				in = new BufferedInputStream(
						new FileInputStream(new File(path)));
				options.inSampleSize = (int) Math.pow(2.0D, i);
//				options.inSampleSize = 1;
				options.inJustDecodeBounds = false;
				bitmap = BitmapFactory.decodeStream(in, null, options);
				break;
			}
			i += 1;
		}
		return bitmap;
	}
	public static  String getSmallPath(){
		smallPath = smallPath;


		return smallPath;
	}




}
