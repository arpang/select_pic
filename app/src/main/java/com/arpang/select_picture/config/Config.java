package com.arpang.select_picture.config;

import android.os.Environment;

import java.io.File;

/**
 * 配置类
 */
public class Config {
    /**
     * 图片存储路径
     */
    public static File FILE_SDCARD = Environment.getExternalStorageDirectory();
    public static String IMAGE_PATH = "Pictures";
    public static File FILE_LOCAL = new File(FILE_SDCARD, IMAGE_PATH);
    public static File FILE_PIC_SCREENSHOT = new File(FILE_LOCAL, "photo");

}
